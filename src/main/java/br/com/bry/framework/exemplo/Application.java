package br.com.bry.framework.exemplo;

import br.com.bry.framework.exemplo.verifier.TimeStampVerifier;

public class Application {

	/**
	 * Run the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		TimeStampVerifier timeStampVerifier = new TimeStampVerifier();

		System.out.println(
				"=====================================Starting time stamp verification by sending original document ...=====================================");

		// The following method will verify the timestamp by sending original document
		timeStampVerifier.verifyBySendingOriginalDocument();

		System.out.println();

		System.out.println(
				"=============Starting time stamp verification by sending hash of the original document ... =============");

		// The following method will verify the timestamp by sending hash of the
		// original document
		timeStampVerifier.verifyBySendingHashOfOriginalDocument();

		System.out.println();

		System.out.println(
				"=============Starting time stamp verification without sending original document data ... =============");

		// The following method will verify the timestamp without sending original
		// document data
		timeStampVerifier.verify();
	}

}
