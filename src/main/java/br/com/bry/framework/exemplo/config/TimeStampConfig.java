package br.com.bry.framework.exemplo.config;

import java.io.File;

public class TimeStampConfig {

	private static final String PATH_SEPARATOR = File.separator;

	// Request identifier
	public static final int NONCE = 1;

	// Identifier of the time stamp within a batch
	public static final int NONCE_OF_TIME_STAMP = 1;

	// Available values: 'BASIC' = Report without certificate chain information and
	// 'CHAIN' = Report with certificate chain information .
	public static final String MODE = "CHAIN";

	// Available values: 'true' = Report with content of the certificate, chain e
	// time stamp encoded in Base64 and 'false' = Report without content of
	// certificate, chain and time stamp encoded in Base64
	public static final boolean CONTENT_RETURN = true;

	// location where the time stamp is stored
	public static final String TIME_STAMP_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "arquivos" + PATH_SEPARATOR
			+ "timestamp-teste1.tst";

	// location where the document is stored
	public static final String DOCUMENT_CONTENT_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "arquivos" + PATH_SEPARATOR
			+ "logoBRy.png";

	// hash of document content
	public static final String DOCUMENT_HASH = "2CAF281193A7580B88C0A15FE0C9AA7A6E940EA86BB0CC2917EF71229CA49393";

}
