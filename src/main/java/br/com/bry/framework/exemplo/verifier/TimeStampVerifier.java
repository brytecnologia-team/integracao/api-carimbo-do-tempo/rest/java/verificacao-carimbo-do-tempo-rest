package br.com.bry.framework.exemplo.verifier;

import java.io.File;

import br.com.bry.framework.exemplo.config.ServiceConfig;
import br.com.bry.framework.exemplo.config.TimeStampConfig;
import br.com.bry.framework.exemplo.util.ResponseWriter;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class TimeStampVerifier {

	private String token = ServiceConfig.ACCESS_TOKEN;

	private static final String URL_TIMESTAMP_VERIFIER = ServiceConfig.URL_TIMESTAMP_VERIFIER;

	/**
	 * Verifies the timestamp by sending original document
	 */
	public void verifyBySendingOriginalDocument() {

		try {

			// Validates the access token
			validateAccessToken();

			// Configures the request
			RequestSpecification requestSpec = this.createVerifyRequestBySendingOriginalDocument();

			// Perform communication with the API
			Object verifierResponse = this.connectToService(requestSpec);

			// Prints timestamp report
			new ResponseWriter().write(verifierResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Verifies the timestamp by sending hash of original document
	 */
	public void verifyBySendingHashOfOriginalDocument() {

		try {
			// Validates the access token
			validateAccessToken();

			// Configures the request
			RequestSpecification requestSpec = this.createVerifyRequestBySendingHashOfOriginalDocument();

			// Perform communication with the API
			Object verifierResponse = this.connectToService(requestSpec);

			// Prints timestamp report
			new ResponseWriter().write(verifierResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Verifies the timestamp without sending original document data
	 */
	public void verify() {

		try {
			// Validates the access token
			validateAccessToken();

			// Configures the request
			RequestSpecification requestSpec = this.createVerifyRequest();

			// Perform communication with the API
			Object verifierResponse = this.connectToService(requestSpec);

			// Prints timestamp report
			new ResponseWriter().write(verifierResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Configures the timestamp request by sending original document
	 * 
	 * @return Timestamp request
	 */
	private RequestSpecification createVerifyRequestBySendingOriginalDocument() {
		String timeStampFilePath = TimeStampConfig.TIME_STAMP_PATH;
		String documentFilePath = TimeStampConfig.DOCUMENT_CONTENT_PATH;

		RequestSpecBuilder builder = new RequestSpecBuilder();

		int timestampQuantitiesToVerify = 1;

		for (int indexOfTimeStamp = 0; indexOfTimeStamp < timestampQuantitiesToVerify; indexOfTimeStamp++) {
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][nonce]", Integer.toString(indexOfTimeStamp));
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][content]", new File(timeStampFilePath));
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][documentContent]", new File(documentFilePath));
		}

		builder.addMultiPart("nonce", String.valueOf(TimeStampConfig.NONCE));
		builder.addMultiPart("mode", TimeStampConfig.MODE);
		builder.addMultiPart("contentReturn", String.valueOf(TimeStampConfig.CONTENT_RETURN));

		return builder.build();
	}

	/**
	 * Configures the timestamp request by sending original document hash
	 * 
	 * @return Timestamp request
	 */
	private RequestSpecification createVerifyRequestBySendingHashOfOriginalDocument() {
		String timeStampFilePath = TimeStampConfig.TIME_STAMP_PATH;

		RequestSpecBuilder builder = new RequestSpecBuilder();

		int timestampQuantitiesToVerify = 1;

		for (int indexOfTimeStamp = 0; indexOfTimeStamp < timestampQuantitiesToVerify; indexOfTimeStamp++) {
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][nonce]", Integer.toString(indexOfTimeStamp));
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][content]", new File(timeStampFilePath));
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][documentHash]", TimeStampConfig.DOCUMENT_HASH);
		}

		builder.addMultiPart("nonce", String.valueOf(TimeStampConfig.NONCE));
		builder.addMultiPart("mode", TimeStampConfig.MODE);
		builder.addMultiPart("contentReturn", String.valueOf(TimeStampConfig.CONTENT_RETURN));

		return builder.build();
	}

	/**
	 * Configures the timestamp request without sending original document data
	 * 
	 * @return Timestamp request
	 */
	private RequestSpecification createVerifyRequest() {
		String timeStampFilePath = TimeStampConfig.TIME_STAMP_PATH;

		RequestSpecBuilder builder = new RequestSpecBuilder();

		int timestampQuantitiesToVerify = 1;

		for (int indexOfTimeStamp = 0; indexOfTimeStamp < timestampQuantitiesToVerify; indexOfTimeStamp++) {
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][nonce]", Integer.toString(indexOfTimeStamp));
			builder.addMultiPart("timestamps[" + indexOfTimeStamp + "][content]", new File(timeStampFilePath));
		}

		builder.addMultiPart("nonce", String.valueOf(TimeStampConfig.NONCE));
		builder.addMultiPart("mode", TimeStampConfig.MODE);
		builder.addMultiPart("contentReturn", String.valueOf(TimeStampConfig.CONTENT_RETURN));

		return builder.build();
	}

	/**
	 * Validates the configured access token
	 * 
	 * @throws Exception
	 */
	private void validateAccessToken() throws Exception {

		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			throw new Exception("Set up a valid token!");
		}
	}

	/**
	 * Perform communication with the API to verify the timestamp
	 * 
	 * @param requestSpec
	 * @return Timestamp object answer
	 */
	private Object connectToService(RequestSpecification requestSpec) {
		return RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec).expect().statusCode(200).when()
				.post(URL_TIMESTAMP_VERIFIER).as(Object.class);
	}

}
