package br.com.bry.framework.exemplo.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResponseWriter {

	/**
	 * Prints the timestamp report
	 * 
	 * @param verifierResponse
	 * @throws JSONException
	 */
	public void write(Object verifierResponse) throws JSONException {

		String jsonResponse = ConverterUtil.convertObjectToJSON(verifierResponse);

		System.out.println("JSON response of time stamp verification: " + jsonResponse);

		JSONObject reports = new JSONObject(new JSONObject(jsonResponse).getJSONArray("reports").getString(0));

		if (!getString(reports, "key").equals("")) {
			System.out.println("Time stamp report response key: " + reports.getString("key"));
		}
		if (!getString(reports, "message").equals("")) {
			System.out.println("Time stamp report response message: " + reports.getString("message"));
		}

		JSONObject timeStampStatus = reports.getJSONObject("timeStampStatus");

		String status = timeStampStatus.getString("status");
		System.out.println("Time stamp status: " + status);

		this.printKeyDescriptionAlertsIfExists(timeStampStatus);
		this.printKeyDescriptionErrorsIfExists(timeStampStatus);
		this.printKeyDescriptionInfosIfExists(timeStampStatus);

		System.out.println("Hash of stamped content: " + timeStampStatus.getString("timeStampContentHash"));

		System.out.println("Time stamp date: " + timeStampStatus.getString("timeStampDate"));

		System.out.println("Time stamp serial number: " + timeStampStatus.getString("timestampSerialNumber"));

		System.out.println("Time stamp policy: " + timeStampStatus.getString("timeStampPolicy"));

	}

	private void printKeyDescriptionInfosIfExists(JSONObject timeStampStatus) throws JSONException {
		JSONObject infoStatus = null;
		try {
			infoStatus = timeStampStatus.getJSONObject("infoStatus");
			JSONArray keyDescriptionList = infoStatus.getJSONArray("keyDescriptionList");
			System.out.println("Info description list:");
			extractKeyMessage(keyDescriptionList);
		} catch (JSONException e) {
			// Key description is empty.
		}
	}

	private void printKeyDescriptionErrorsIfExists(JSONObject timeStampStatus) throws JSONException {
		JSONObject errorStatus = null;
		try {
			errorStatus = timeStampStatus.getJSONObject("errorStatus");
			JSONArray keyDescriptionList = errorStatus.getJSONArray("keyDescriptionList");
			System.out.println("Error description list:");
			extractKeyMessage(keyDescriptionList);
		} catch (JSONException e) {
			// Key description is empty.
		}

	}

	private void printKeyDescriptionAlertsIfExists(JSONObject timeStampStatus) throws JSONException {
		JSONObject alertStatus = null;
		try {
			alertStatus = timeStampStatus.getJSONObject("alertStatus");
			JSONArray keyDescriptionList = alertStatus.getJSONArray("keyDescriptionList");
			System.out.println("Alert description list:");
			extractKeyMessage(keyDescriptionList);
		} catch (JSONException e) {
			// Key description is empty.
		}

	}

	private void extractKeyMessage(JSONArray keyDescriptionList) throws JSONException {
		for (int i = 0; i < keyDescriptionList.length(); i++) {
			JSONObject keyDescriptionItem = new JSONObject(keyDescriptionList.getString(i));
			System.out.print("  key #" + i + " : " + keyDescriptionItem.getString("key"));
			System.out.println(" - message #" + i + " : " + keyDescriptionItem.getString("description"));
		}
	}

	private String getString(JSONObject object, String key) {
		String string = null;
		try {
			string = object.getString(key);
		} catch (JSONException e) {
			string = "";
		}
		return string;

	}
}
