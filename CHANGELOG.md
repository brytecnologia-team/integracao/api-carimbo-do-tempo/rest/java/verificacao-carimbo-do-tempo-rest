# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-06-23

### Added

- Code comments.

## [1.0.0] - 2020-06-22

### Added

- Examples of timestamp verification. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/verificacao-carimbo/-/tags/1.0.0
[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/verificacao-carimbo/-/tags/1.0.1